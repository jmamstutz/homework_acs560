package HW_07;

import java.util.ArrayList;

public class Game {
	protected Scorer s = new Scorer();
	
	private int frameIncrement;
	private int listFrameIncrement;
	private int frameSum;
	private int strikeCombo;

	private boolean strikeBonus;
	private boolean spareBonus;

	public int score() { // Return current score
		ArrayList<Integer> finalScoreList = new ArrayList<Integer>();
		int totalScore = 0;
		
		finalScoreList = s.gameFrames;
		while(finalScoreList.isEmpty() == false) {
			totalScore += finalScoreList.remove(0);
		}
		
		return totalScore;
	}
	
	public void add(int pins) { // Record # pins for a throw
		frameSum += pins;
		frameIncrement += 1;
			
		if(frameIncrement % 2 != 0 || frameIncrement == 0) { // First half of a normal frame
			if(spareBonus == true) { // Add first value of current frame to sum of last frame
				s.gameFrames.set(listFrameIncrement - 1,
					frameSum + s.returnFrameScore(listFrameIncrement - 1));
				spareBonus = false;
			}
				
			if(pins == 10) { // A strike
				frameIncrement += 1;

				if(strikeBonus == true) { // Add sum of current frame to sum of last frame
					strikeCombo++;
					
					if(strikeCombo < 11) {
						s.gameFrames.set(listFrameIncrement - 1,
							frameSum + s.returnFrameScore(listFrameIncrement - 1));
					}	
						
					if(strikeCombo >= 2 && strikeCombo < 10) { // A triple strike/"turkey"
						s.gameFrames.set(listFrameIncrement - 2,
							frameSum + s.returnFrameScore(listFrameIncrement - 2));				
					}
				}
				strikeBonus = true;
					
				listFrameIncrement++;
				s.recordThrow(frameSum);
				frameSum = 0;
				return;
			}	
				
			if(strikeCombo >= 1 && strikeCombo < 10) { // A double strike/"rhino"
				s.gameFrames.set(listFrameIncrement - 2,
					frameSum + s.returnFrameScore(listFrameIncrement - 2));
			}
			if(strikeCombo >= 10) {
				listFrameIncrement++;
				s.recordThrow(frameSum);
				frameSum = 0;			
			}
			strikeCombo = 0;
				
		} else { // Second half of a normal frame				
			if(frameSum == 10) { // A spare
				spareBonus = true;
			}
				
			if(strikeBonus == true) { // Add sum of current frame to sum of last frame
				s.gameFrames.set(listFrameIncrement - 1,
					frameSum + s.returnFrameScore(listFrameIncrement - 1));				
				strikeBonus = false;	
			}
				
			listFrameIncrement++;
			s.recordThrow(frameSum);
			frameSum = 0;
		}	
	}
}
