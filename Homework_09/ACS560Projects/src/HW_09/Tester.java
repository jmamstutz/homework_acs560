package HW_09;

import org.junit.Assert;
import org.junit.Test;

public class Tester {

	@Test
	public void test() {
		int[] expectedResult1 = {}; // Max value is 1, so no prime numbers should return
		int[] actualResult = PrimeGenerator.generatePrimes(1);
		Assert.assertArrayEquals(expectedResult1, actualResult);
		
		int[] expectedResult2 = {2}; // Max value is 2, so array should just return 2
		actualResult = PrimeGenerator.generatePrimes(2);
		Assert.assertArrayEquals(expectedResult2, actualResult);

		int[] expectedResult3 = {2,3,5}; // Max value is 5, so array should return first three primes
		actualResult = PrimeGenerator.generatePrimes(5);
		Assert.assertArrayEquals(expectedResult3, actualResult);
		
		/* The above usually fails the unit test because of a bug found in the
		 * crossOutMultiples method. To fix it, in the for loop that goes
		 * up to the iteration limit, have the upper bounds be i <= limit
		 * instead of i < limit. In this way the multiple of 2 (4) is
		 * crossed out and not included in the returned array
		 */
		
		int[] expectedResult4 = {2,3,5}; // Max value is 6, so array should be identical to the last test
		actualResult = PrimeGenerator.generatePrimes(6);
		Assert.assertArrayEquals(expectedResult4, actualResult);	
		
		/* The above usually fails for the same reasons as the test case above,
		 * except this time there is a 6 added to the end of the array as well
		 * as the 4 from before. Making the same fix as the last example regarding
		 * making i <= limit will correct this test case
		 */
		
		int[] expectedResult5 = {2,3,5,7}; // Max value is 10, so array should include four values
		actualResult = PrimeGenerator.generatePrimes(10);
		Assert.assertArrayEquals(expectedResult5, actualResult);
		
		/* Incorrect for the same reasons as the last two test cases, except this
		 * time it concerns a multiple of 3 (9) instead of a multiple of two. Once
		 * more this bug is fixed by tweaking the for loop so its upper limit before 
		 * it terminates is i <= limit
		 */
		
		int[] expectedResult6 = {2,3,5,7,11,13}; // Max value is 15, so array should include six values
		actualResult = PrimeGenerator.generatePrimes(15);
		Assert.assertArrayEquals(expectedResult6, actualResult);
		
		int[] expectedResult7 = {2,3,5,7,11,13,17,19}; // Max value is 20, so array should include eight values
		actualResult = PrimeGenerator.generatePrimes(20);
		Assert.assertArrayEquals(expectedResult7, actualResult);
		
		int[] expectedResult8 = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97}; // Max value is 100, so array should include 34 values
		actualResult = PrimeGenerator.generatePrimes(100);
		Assert.assertArrayEquals(expectedResult8, actualResult);
		
		int[] expectedResult9 = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139}; // Max value is 143, so array should include 25 values
		actualResult = PrimeGenerator.generatePrimes(143);
		Assert.assertArrayEquals(expectedResult9, actualResult);
		
		/* Test cases consistently appear to return positive beyond max values
		 * of 15
		 */
		
		/* The vast majority, if not all of the bugs of the class result
		 * from the crossOutMultiples method failing to account for previously unseen
		 * multiples of a number AT the imposed limit set by the determineIterationLimit
		 * method. This is fixed by making i <= limit
		 */
		
	}

}
