package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./p4.db")

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS COURSES (Semester TEXT, Course TEXT)")
	statement.Exec()

	statement, _ = database.Prepare("INSERT INTO COURSES (Semester, Course) VALUES (?, ?)")
	statement.Exec("Fall 2018", "Software Engineering")
	statement, _ = database.Prepare("INSERT INTO COURSES (Semester, Course) VALUES (?, ?)")
	statement.Exec("Spring 2018", "Software Project Management")

	rows, _ := database.Query("SELECT * FROM COURSES")
	var firsttext string
	var secondtext string
	for rows.Next() {
		rows.Scan(&firsttext, &secondtext)
		fmt.Println(firsttext + " " + secondtext)
	}

	database.Close()
}
