package HW_07;

import java.util.ArrayList;

public class Scorer {
	protected ArrayList<Integer> gameFrames = new ArrayList<Integer>();

	public void recordThrow(int throwScore) { // Store # of pins for a throw
		gameFrames.add(throwScore);
	}
	public int returnFrameScore(int framePos) { // Calculate score of a given frame
		return gameFrames.get(framePos);
	}
}
