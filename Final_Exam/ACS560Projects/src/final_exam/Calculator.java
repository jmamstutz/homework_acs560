package final_exam;

// This is the fixed version of the Calculator class for the Final Exam

public class Calculator extends RuntimeException {
	public Calculator() {}
	
	public static int CalculatorFromReversePolish(String expression) {
		int result = 0;
		int op1 = 0;
		int op2 = 0;
		
		Stack<Integer> stack = new Stack<Integer>();
		String tokens[] = expression.split(" ");	
		
		if (tokens.length == 0)
			throw new RuntimeException();
		
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("+")) {
				stack.push(new Integer((Integer)stack.pop() + (Integer)stack.pop()));
			}
			else if (tokens[i].equals("*")) {
				stack.push(new Integer((Integer)stack.pop() * (Integer)stack.pop()));
			}
			else if (tokens[i].equals("-")) {
				op1 = (Integer)stack.pop();
				op2 = (Integer)stack.pop();
				stack.push(op2-op1);
			}
			else if (tokens[i].equals("/")) {
				op1 = (Integer)stack.pop();
				op2 = (Integer)stack.pop();
				stack.push(op2/op1);
			}
			else {
				try {
					Integer op = Integer.parseInt(tokens[i]);
					stack.push(op);
				}
				catch (NumberFormatException e) {}
			}
		}		
		
		if (stack.isEmpty())
			throw new RuntimeException();
		
		result = stack.pop();
		
		if (!stack.isEmpty())
			throw new RuntimeException();
		
		return result;
	}
}
