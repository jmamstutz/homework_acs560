package final_exam;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTester {

	@Test
	public void test() {
		int expectedResult1 = -9;
		int actualResult = Calculator.CalculatorFromReversePolish("1 2 - 4 5 + *");
		Assert.assertEquals(expectedResult1, actualResult);
		
		int expectedResult2 = 2;
		actualResult = Calculator.CalculatorFromReversePolish("4 2 /");
		Assert.assertEquals(expectedResult2, actualResult);
		
		int expectedResult3 = -5;
		actualResult = Calculator.CalculatorFromReversePolish("5 5 + 7 9 - /");
		Assert.assertEquals(expectedResult3, actualResult);
		
		int expectedResult4 = -1;
		actualResult = Calculator.CalculatorFromReversePolish("1 1 1 1 + - *");
		Assert.assertEquals(expectedResult4, actualResult);
		
		int expectedResult5 = 6;
		actualResult = Calculator.CalculatorFromReversePolish("5 6 * 5 /");
		Assert.assertEquals(expectedResult5, actualResult);
		
		/* The primary bug(s) found were the Calculator class being incapable
		 * of properly handling the - and / operands. It would use the wrong
		 * order of the two subtracted or divided values for determining a
		 * result. Therefore, the class has been altered so that the values
		 * are retrieved first from the Stack being popped twice, subtracted
		 * or divided in the correct sequence, then pushed back onto the Stack.
		 * This issue is not relevant to adding or multiplying any values
		 */
	}

}
