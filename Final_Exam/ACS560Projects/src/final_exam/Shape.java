package final_exam;

abstract class Shape {
	protected double width;
	protected double height;
	
	abstract double area(); 
	
	protected Shape(double width, double height) {
		this.width = width;		
		this.height = height;
	}
}

class Square extends Shape {
	protected Square(double width, double height) {
		super(width, height);
	}

	double area() {
		return width * width;
	}
}

class Circle extends Shape {
	protected Circle(double width, double height) {
		super(width, height);
	}

	double area() {
		return Math.PI * width * width /4.0;
	}
}

class RightTriangle extends Shape {
	protected RightTriangle(double width, double height) {
		super(width, height);
	}

	double area() {
		return width * width /2.0;
	}
}

class Rectangle extends Shape {
	protected Rectangle(double width, double height) {
		super(width, height);
	}

	double area() {
		return width * height;
	}
}

class IsoscelesTriangle extends Shape {
	protected IsoscelesTriangle(double width, double height) {
		super(width, height);
	}

	double area() {
		return width * height /2.0;
	}
}

class Ellipse extends Shape {
	protected Ellipse(double width, double height) {
		super(width, height);
	}

	double area() {
		return Math.PI * width * height;
	}
}
