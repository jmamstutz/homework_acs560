package final_exam;

// Tests various shapes using polymorphism for Part 2 of Final Exam
public class ShapeTester {
	public static void main(String[] args) {
		Square sq = new Square(4,0);
		System.out.println("The area of a Square of size " + sq.width + " is " 
			+ sq.area());
		
		Circle ci = new Circle(6,0);
		System.out.println("The area of a Circle of radius " + ci.width + " is " 
			+ ci.area());
		
		RightTriangle rt = new RightTriangle(3,0);
		System.out.println("The area of a Right Triangle of size " + rt.width + " is " 
			+ rt.area());
		
		Rectangle rc = new Rectangle(3,4);
		System.out.println("The area of a Rectangle of width " + rc.width + " and height "
			+ rc.height + " is "  + rc.area());
		
		IsoscelesTriangle it = new IsoscelesTriangle(5,8);
		System.out.println("The area of an Isosceles Tringle of width " + it.width + " and height "
			+ it.height + " is "  + it.area());
		
		Ellipse el = new Ellipse(5,2);
		System.out.println("The area of an Ellipse of first radius " + el.width + " and second radius "
			+ el.height + " is "  + el.area());
	}
}
