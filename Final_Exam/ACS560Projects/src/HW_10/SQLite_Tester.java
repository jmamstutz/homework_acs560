package HW_10;

import java.sql.*;

public class SQLite_Tester {

	public static void main(String[] args) {
	    Connection c = null;
	    Statement stmt = null;
	    
		try {
			// Creates database file
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:p2.db");
	        c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			
			// Creates table within database
			stmt = c.createStatement();
			String sql = "CREATE TABLE COURSES " +
				"(SEMESTER       TEXT    NOT NULL, " + 
				" COURSE         TEXT     NOT NULL)";
			stmt.executeUpdate(sql);
			stmt.close();
			
			// Inserts two records into table
			stmt = c.createStatement();
		    sql = "INSERT INTO COURSES (SEMESTER,COURSE) " +
		    	"VALUES ('Fall 2018', 'Software Engineering' );"; 
		    stmt.executeUpdate(sql);

		    sql = "INSERT INTO COURSES (SEMESTER,COURSE) " +
		    	"VALUES ('Spring 2018', 'Software Project Management' );"; 
		    stmt.executeUpdate(sql);
			stmt.close();
	        c.commit();
		    
	        // Retrieves and displays table with records here
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM COURSES;" );
		      
			while ( rs.next() ) {
				String semester = rs.getString("semester");
				String course  = rs.getString("course");
			         
				System.out.println( "Semester = " + semester );
				System.out.println( "Course = " + course );
			}
			rs.close();
			stmt.close();
			c.close();
			
			System.out.println("Table and records created successfully");
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}	
}