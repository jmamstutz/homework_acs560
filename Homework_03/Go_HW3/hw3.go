package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
)

func main() {
	var buffer bytes.Buffer
	increment := 0
	data := []string{"Semester", "Course", "Fall 2018", "Software Engineering", "Fall 2018", "Advanced Computer Networks"}

	file, err := os.Create("hw3.html")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer file.Close()

	buffer.WriteString("<html>\r<head>\r</head>\r<body>\r")
	buffer.WriteString("<table border='1px' width='100%>' cellpadding='1' cellspacing='1'>\r")

	for _, v := range data {
		if increment == 0 {
			buffer.WriteString("<tr>\r")
		}

		buffer.WriteString("<td>")
		buffer.WriteString(v)
		buffer.WriteString("</td>\r")

		if increment == 1 {
			buffer.WriteString("</tr>\r")
			increment = 0
		} else {
			increment++
		}
	}

	buffer.WriteString("</table>\r</body>\r</html>")

	file.WriteString(buffer.String())
	fmt.Printf("Completed creating hw3.html")
}
