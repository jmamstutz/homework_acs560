﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

// The resulting hw3.html file appears in the main folder of the HTML_HW3 directory 

namespace CSharp_HW3 {
    class Program {

        protected string GenerateHTML(DataTable dt) {
            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<head>");
            builder.Append("</head>");
            builder.Append("<body>");
            builder.Append("<table border='1px' width='100%' cellpadding='1' cellspacing='1'>");

            builder.Append("<tr>");
            foreach (DataColumn myColumn in dt.Columns)
            {
                builder.Append("<td>");
                builder.Append(myColumn.ColumnName);
                builder.Append("</td>");
            }
            builder.Append("</tr>");

            foreach (DataRow myRow in dt.Rows)
            {
                builder.Append("<tr>");
                foreach (DataColumn myColumn in dt.Columns)
                {
                    builder.Append("<td>");
                    builder.Append(myRow[myColumn.ColumnName].ToString());
                    builder.Append("</td>");
                }
                builder.Append("</tr>");
            }

            builder.Append("</table>");
            builder.Append("</body>");
            builder.Append("</html>");

            string result = builder.ToString();
            return result;
        }

        static void Main(string[] args) {
            Program pg = new Program();

            DataTable table = new DataTable();
            table.Clear();
            table.Columns.Add("Semester");
            table.Columns.Add("Course");
            DataRow row1 = table.NewRow();
            row1["Semester"] = "Fall 2018";
            row1["Course"] = "Software Engineering";
            table.Rows.Add(row1);
            DataRow row2 = table.NewRow();
            row2["Semester"] = "Fall 2018";
            row2["Course"] = "Advanced Computer Networks";
            table.Rows.Add(row2);

            string htmlResult = pg.GenerateHTML(table);

            OperatingSystem os = Environment.OSVersion;
            PlatformID pid = os.Platform;
            switch(pid)
            {
                case PlatformID.MacOSX:
                    System.IO.File.WriteAllText(@"../../.././hw3.HTML", htmlResult);
                    break;
                case PlatformID.Unix:
                    System.IO.File.WriteAllText(@"../../.././hw3.HTML", htmlResult);
                    break;
                default:
                    System.IO.File.WriteAllText(@"..\..\..\.\hw3.HTML", htmlResult);
                    break;
            }
        }
    }
}
