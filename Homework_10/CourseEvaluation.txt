.header on
.mode column

create table CourseEvaluation(CourseID int primary key not null, ProfessorID int, Evaluation double);
insert into CourseEvaluation values(1, 1, 4.0);
insert into CourseEvaluation values(2, 2, 3.5);
insert into CourseEvaluation values(3, 3, 4.0);
insert into CourseEvaluation values(4, 4, 3.3);
insert into CourseEvaluation values(5, 5, 3.0);
insert into CourseEvaluation values(6, 6, 2.0);
insert into CourseEvaluation values(7, 7, 2.8);
insert into CourseEvaluation values(8, 8, 3.5);
insert into CourseEvaluation values(9, 9, 3.9);
insert into CourseEvaluation values(10, 10, 4.0);
insert into CourseEvaluation values(11, 10, 3.9);
insert into CourseEvaluation values(12, 10, 3.8);
insert into CourseEvaluation values(13, 11, 4.0);
insert into CourseEvaluation values(14, 8, 2.1);

select * from CourseEvaluation;