James ACS560 Score Report

Homework1 - 100/100
Homework2 - 100/100
Homework3 - 95/100
Homework5 - 98/100
Homework7 - 150/150
Homework8 - 100/100
Homework9 - 100/100
Homework10 - 90/100
 

Note: 
HW1: For C#, always provide total project folder. 
HW3: C#(-5 pts) To implement DRY, use an array to add all courses and iterate them in the HTML.
HW5: (-2 pts) 
Class Diagram:
 Incorrect Cardinality

     Rental *----->1 Movie

HW10: Part I Q2(-10 pts)
      The query submitted doesn't compute the average evaluation score
select professorid, avg(evaluation) avgprof from CourseEvaluation group by professorid
having avgprof > (select avg(profeva) aveeva from (select professorid, avg(evaluation) profeva
from courseevaluation group by professorid)); 

==================================

Course Project:
Homework4 (Requirements) - 100/100  Great job!
Homework6 (System Modeling) - 100/100
