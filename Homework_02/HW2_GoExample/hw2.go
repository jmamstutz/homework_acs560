package main

import (
	"fmt"
	"strconv"
)

func Abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func Sqrt(x float64) float64 {
	z := float64(1)
	previousZ := z
		    
	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		
		if previousZ != z && Abs(previousZ - z) > 0.000000000000001 {
		    fmt.Println(z)	
		    previousZ = z
		} else {
		    fmt.Println("Stopped early at " + strconv.Itoa(i+1) + " iteration(s).")
		    break
		}
	}
	return z
}

func main() {
	fmt.Print(Sqrt(2))
	fmt.Print(" is the final approximate value.")
}
