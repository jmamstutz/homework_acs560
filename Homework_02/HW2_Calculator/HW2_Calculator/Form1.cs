﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW2_Calculator {

    public partial class Calculator : Form {

        double savedResult;
        Boolean hasOperatorStarted = false;
        String currentOperator = "";

        public Calculator() {
            InitializeComponent();
        }

        private void Calculator_Load(object sender, EventArgs e) {

        }

        private void number_pressed(object sender, EventArgs e) {
            if(text_result.Text == "0" || hasOperatorStarted == true) { text_result.Clear(); }

            hasOperatorStarted = false;

            Button currentButton = (Button)sender;
            if (text_result.Text.Contains(".") && currentButton.Text == ".") { return; }

            text_result.Text = text_result.Text + currentButton.Text;
        }

        private void clear_pressed(object sender, EventArgs e) {
            text_result.Text = "0";
            savedResult = 0;
        }

        private void operation_pressed(object sender, EventArgs e) {
            Button currentButton = (Button)sender;

            if (savedResult != 0)
            {
                buttonEquals.PerformClick();

            } else
            {
                savedResult = double.Parse(text_result.Text);
            }
            currentOperator = currentButton.Text;
            hasOperatorStarted = true;
        }

        private void equals_pressed(object sender, EventArgs e) {
            double finalResult;
  
            switch(currentOperator) {
                case "+":
                    finalResult = savedResult + double.Parse(text_result.Text);
                    break;
                case "-":
                    finalResult = savedResult - double.Parse(text_result.Text);
                    break;
                case "*":
                    finalResult = savedResult * double.Parse(text_result.Text);
                    break;
                case "/":
                    finalResult = savedResult / double.Parse(text_result.Text);
                    break;
                default:
                    finalResult = savedResult;
                    break;
            }
            text_result.Text = finalResult.ToString();

            savedResult = finalResult;
            currentOperator = "";
        }
    }
}
