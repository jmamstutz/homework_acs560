package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", index_handler)
	http.ListenAndServe(":12000", nil)
}

func index_handler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello ACS560!")
}
