// Server.go
// This example is from the following link:
// https://systembash.com/a-simple-go-tcp-server-and-tcp-client/

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("Launching server...")

	// listen on all interfaces
	ln, _ := net.Listen("tcp", ":12000")

	// run loop forever (or until ctrl-c)
	for {
		// accept connection on port
		conn, _ := ln.Accept()

		// will listen for message to process ending in newline (\n)
		url, _ := bufio.NewReader(conn).ReadString('\n')
		url = strings.TrimSuffix(url, "\n")

		// output message received
		fmt.Print("Message Received:", string(url))

		resp, err := http.Get(url)
		// handle the error if there is one
		if err != nil {
			panic(err)
		}

		// do this now so it won't be forgotten
		defer resp.Body.Close()

		// reads html as a slice of bytes
		html, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		// send new string back to client
		conn.Write([]byte(string(html) + "\n`"))
	}
}
