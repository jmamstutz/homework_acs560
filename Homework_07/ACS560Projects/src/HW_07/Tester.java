package HW_07;

public class Tester {

	/* 
	 * Can go up to twelve frames max (9 normal frames, last frame in
	 * three parts when attempting a perfect game of 300 points)
	 */
	
	Game g;
	int testCounter = 1;
	
	public static void main(String[] args) { 
		Tester t = new Tester();
		
		t.oneFrameTest();
		t.noSpareStrikeTest();
		t.firstSpareTest();
		t.secondSpareTest();
		t.firstStrikeTest();
		t.secondStrikeTest();
		t.doubleStrikeTest();
		t.tripleStrikeTest();
		t.multipleStrikeTest();
		t.firstCloseToPerfectTest();
		t.secondCloseToPerfectTest();
		t.justShyTest();
		t.perfectGameTest();
	}
	
	public void oneFrameTest() { // Result should be 9
		g = new Game();
		g.add(5);
		g.add(4);
		getScore();		
	}
	
	public void noSpareStrikeTest() { // Result should be 56
		g = new Game();
		g.add(1);
		g.add(4);
		g.add(2);
		g.add(5);
		g.add(3);
		g.add(6);
		g.add(4);
		g.add(4);
		g.add(5);
		g.add(1);
		g.add(0);
		g.add(0);
		g.add(2);
		g.add(7);
		g.add(3);
		g.add(3);
		g.add(4);
		g.add(2);
		getScore();		
	}
	
	public void firstSpareTest() { // Result should be 18
		g = new Game();
		g.add(3);
		g.add(7);
		g.add(3);
		g.add(2);
		getScore();			
	}
	
	public void secondSpareTest() { // Result should be 24
		g = new Game();
		g.add(0);
		g.add(10);
		g.add(5);
		g.add(4);
		getScore();		
	}
	
	public void firstStrikeTest() { // Result should be 28
		g = new Game();
		g.add(10);
		g.add(3);
		g.add(6);	
		getScore();		
	}
	
	public void secondStrikeTest() { // Result should be 25
		g = new Game();
		g.add(4);
		g.add(5);
		g.add(10);	
		g.add(2);	
		g.add(1);
		getScore();		
	}
	
	public void doubleStrikeTest() { // Result should be 57
		g = new Game();
		g.add(10);
		g.add(10);
		g.add(9);
		g.add(0);
		getScore();	
	}
	
	public void tripleStrikeTest() { // Result should be 104
		g = new Game();
		g.add(10);
		g.add(10);
		g.add(10);
		g.add(8);
		g.add(2);
		g.add(8);
		g.add(0);
		getScore();	
	}
	
	public void multipleStrikeTest() { // Result should be 145
		g = new Game();
		g.add(10);
		g.add(10);
		g.add(10);
		g.add(10);
		g.add(10);
		g.add(7);
		g.add(2);
		getScore();	
	}
	
	public void firstCloseToPerfectTest() { // Result should be 272
		g = new Game();
		for(int i = 0; i < 9; i++) {
			g.add(10);		
		}
		g.add(9);
		g.add(1);
		g.add(3);
		getScore();			
	}
	
	public void secondCloseToPerfectTest() { // Result should be 250
		g = new Game();
		for(int i = 0; i < 9; i++) {
			g.add(10);		
		}
		g.add(0);
		g.add(5);
		getScore();			
	}
	
	public void justShyTest() { // Result should be 299
		g = new Game();
		for(int i = 0; i < 11; i++) {
			g.add(10);		
		}
		g.add(9);
		getScore();			
	}
	
	public void perfectGameTest() { // Result should be 300
		g = new Game();
		for(int i = 0; i < 12; i++) {
			g.add(10);		
		}
		getScore();			
	}
	
	public void getScore() {
		int result = g.score();
		System.out.println("Final score for game " + testCounter + " is: " + result);	
		testCounter++;
	}

}
